/****************************************************************************************
 *
 * 文件名: main.c
 * 项目名: 02-TIME1_TSET_2
 * 版 本: v1.0
 * 日 期: 2016年05月31日 15时33分45秒
 * 作 者: Administrator
 * 程序说明:16位T1定时例程,使用重载功能
 * 适用芯片: KF8FXXXX系列——KF8F2156、KF8F3156、KF8F4156、KF8F3155、KF8F4155、KF8F4158
 * 			KF8TSXXXX系列——KF8TS2716、KF8TS2516
 * 			KF8VXXX系列——KF8V327、KF8V427、KF8V429
 ****************************************************************************************/
#include<KF8TS2716.h>
/****************************************************************************************
 * 函数名：   init_mcu
 * 函数功能：mcu初始化函数
 * 入口参数：无
 * 返回：       无
 ****************************************************************************************/
void init_mcu()
{
	/***时钟初始化****/
	OSCCTL = 0x60;          //设置为8M
	/***端口初始化****/
	TR0 = 0X04;            //P02设置为输入，其余口为输出
	TR1 = 0x00;            //P1设置为输出
	TR2 = 0X00;            //P2设置为输出
	TR3 = 0X00;            //P3设置为输出

    P0LR=0x00;				//P0输出低
    P1LR=0x00;              //P1输出低
    P2LR=0x00;				//P2输出低
    P3LR=0x00;				//P3输出低

    P0=0x00;
    P1=0x00;
    P2=0x00;
    P3=0x00;
}
/****************************************************************************************
 * 函数名：   init_T1()
 * 函数功能：LED初始化函数
 * 入口参数：无
 * 返回：       无
 ****************************************************************************************/
void init_T1()
{
	PP2 = 0;
	PP1 = 46;
	T1H = 0;
	T1L = 0;	   //定时25us
	T1CTL = 0x81;  //使能重载功能，定时模式，开启计数
	T1IF = 0;      //清除T1中断标志
 	T1IE = 1;	   //T1中断使能
 	PUIE = 1;      //使能外设中断
 	AIE  = 1;      //使能全局变化中断
}
//主函数
void main()
{
	int i=0;
	init_mcu();
	init_T1();
	while(1)
	{
      _CWDT();
	}
}
//高优先中断函数
void int_fun0() __interrupt (0)
{
	if(T1IF)
	{
		T1IF=0;
		P1LR3=!P1LR3;
	}
}
//低优先中断函数
void int_fun1() __interrupt (1)
{

}

