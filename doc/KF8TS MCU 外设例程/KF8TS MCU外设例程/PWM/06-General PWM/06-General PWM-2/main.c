/****************************************************************************************
 *
 * 文  件  名: main.c
 * 项  目  名: 06-General PWM-2
 * 版         本: v1.0
 * 日         期: 2016年05月16日 11时38分22秒
 * 作         者: Administrator
 * 程序说明：普通PWM参考例程
 * 			PWM时钟源可选择T1(Fsys/4)，也可以选择系统时钟Fosc。
 * 适用芯片：KF8VXXX系列——KF8V111、KF8V120、KF8V200、KF8V204、KF8V211、KF8V216、
 * 						 KF8V218、KF8V220、KF8V327、KF8V427、KF8V429
 ****************************************************************************************/
#include<KF8V111.h>

#define uchar unsigned char
#define uint  unsigned int

/****************************************************************************************
 * 函数名     ：Delay_ms
 * 函数功能：长时间延时
 * 入口参数：延时基数 uchar ms_data
 * 返回          ：无
 ****************************************************************************************/
void Delay_ms(uint ms_data)
{
	uchar i;
	while(ms_data--)
	{
		i = 124;
		while(i--);
	}
}

/****************************************************************************************
 * 函数名     ：init_mcu
 * 函数功能：mcu初始化函数
 * 入口参数：无
 * 返回          ：无
 ****************************************************************************************/
void init_mcu()
{
	/***时钟初始化****/
	OSCCTL = 0x50;          //设置为16M

	/***端口初始化****/
	TR0 = 0x08;				//设置P03端口为输入，P0其他I/O口为输出
	TR1 = 0x00;				//设置P1端口为输出
	TR2 = 0x00;				//设置P2端口为输出
	P0 = 0;
	P1 = 0;
	P2 = 0;
	P0LR = 0;				//P0口输出低电平
	P1LR = 0;				//P1口输出低电平
	P2LR = 0;				//P2口输出低电平
}

/****************************************************************************************
 * 函数名     ：init_pwm
 * 函数功能：pwm初始化函数
 * 入口参数：无
 * 返回          ：无
 ****************************************************************************************/
void init_pwm()
{
	/***PWM相关寄存器初始化****/

	//时钟选择T1。A01版本只能选择T1时钟源作为PWM的时钟。
	T1CTL = 0X01;			//T1预分频比=1，启动T1
	T1L = 0;
	T1H = 0;
	PP1 = 0xff;				//PWM1的周期寄存器 ，分辨率为255，T=(255+1)*4*(1/16MHz)*1=64us，F=1/T=15.6KHz
	PP2 = 0xff;				//PWM2的周期寄存器 ，分辨率为255，T=(255+1)*4*(1/16MHz)*1=64us，F=1/T=15.6KHz
	PWM1L = PP1/2;			//PWM1初始化占空比为50%
	PWM2L = PP2/2;			//PWM2初始化占空比为50%

	PWMCTL = 0;				//关闭PWM1、PWM2

//	//选择MCU振荡时钟，通过PWMCTL的第2(PWM1CKS)、3(PWM2CKS)位选择
//	PP1 = 0xff;				//PWM1的周期寄存器 ，分辨率为255，T=(255+1)*(1/16MHz)=16us，F=1/T=62.5KHz
//	PP2 = 0xff;				//PWM2的周期寄存器 ，分辨率为255，T=(255+1)*(1/16MHz)=16us，F=1/T=62.5KHz
//	PWM1L = PP1/2;			//PWM1初始化占空比为50%
//	PWM2L = PP2/2;			//PWM2初始化占空比为50%
//
//	PWM1CKS=1;				//PWM1时钟源选择MCU振荡时钟
//	PWM2CKS=1;				//PWM2时钟源选择MCU振荡时钟
}

/****************************************************************************************
 * 函数名     ：init_pwm
 * 函数功能：pwm功能函数
 * 入口参数：无
 * 返回          ：无
 ****************************************************************************************/
void pwm_function()
{
	static uchar direction=0;
	if(direction)
	{
		PWM1L++;						//PWM1增大占空比
		PWM2L++;						//PWM2增大占空比
		if(PWM1L > (PP1-1) )
		{
			direction=0;				//换向
		}
	}
	else
	{
		PWM1L--;						//PWM1减小占空比
		PWM2L--;						//PWM1减小占空比
		if(PWM1L==0)
		{
			direction=1;				//换向
		}
	}
}

/****************************************************************************************
 * 函数名     ：main
 * 函数功能：程序入口主函数
 * 入口参数：无
 * 返回          ：无
 ****************************************************************************************/
void main()
{
	init_mcu();
	init_pwm();
	Delay_ms(10);

	PWM1ON=1;							//开启PWM1
	PWM2ON=1;							//开启PWM2
	while(1)
	{
		pwm_function();
		Delay_ms(10);
	}
}

//中断函数0:0X04入口地址
void int_fun0() __interrupt (0)
{

}


//中断函数1:0x14入口地址
void int_fun1() __interrupt (1)
{

}
