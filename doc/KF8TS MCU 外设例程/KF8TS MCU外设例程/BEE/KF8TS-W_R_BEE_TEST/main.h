/****************************************************************************************
 * 文件名: main.h
 * 日 期:  2014-6-28
 * 作 者:  上海芯旺微电子技术有限公司
 * 说明：    主函数头文件
 ****************************************************************************************/

#ifndef MAIN_H_
#define MAIN_H_

#include <KF8TS2408.h>
#include "init_mcu.h"
#include "W_BEE.h"

#define uint unsigned int
#define uchar unsigned char

#define		OUT1		P0LR0
#define		OUT2		P0LR1

void delay_ms(uint ms_data);
void delay_us(uint us_data);

#define     TRUE				1
#define		FLASE				0

extern sfr_bits	KF8_BIT_VALUE1;
#define Touch_flag					KF8_BIT_VALUE1._0
#define SYS_KEY_Dimmer_Two_Flag		KF8_BIT_VALUE1._1
#define SYS_Key_Down_Flag			KF8_BIT_VALUE1._2
#define SYS_Key_Up_Flag				KF8_BIT_VALUE1._3
#define SYS_KEY_Dimmer_One_Flag		KF8_BIT_VALUE1._4
#define SYS_WORK_KEY_Count_FLAG		KF8_BIT_VALUE1._5
#define SYS_LONG_Down_Flag			KF8_BIT_VALUE1._6
#define SYS_WORK_10MS_FLAG			KF8_BIT_VALUE1._7

extern sfr_bits	KF8_BIT_VALUE2;
#define Power_ON_Flag				KF8_BIT_VALUE2._0
#define W_BEE_Flag					KF8_BIT_VALUE2._1
#define Power_ON_First_Flag			KF8_BIT_VALUE2._2
#define Power_ON_Two_Flag			KF8_BIT_VALUE2._3
#define Power_ON_Two_First_Flag		KF8_BIT_VALUE2._4
#define Dimmer_One_Flag				KF8_BIT_VALUE2._5
#define Dimmer_Two_Flag				KF8_BIT_VALUE2._6


#define Sent_Vaule_To_UART(key_buf)   {TXSDR=key_buf; while(TXSRS==0);}// 调试时串口输出


#endif /* MAIN_H_ */
