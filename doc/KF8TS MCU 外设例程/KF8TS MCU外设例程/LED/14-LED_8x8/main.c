/****************************************************************************************
 *
 * 文件名: main.c
 * 项目名: 14-LED_8x8
 * 版 本: v1.0
 * 日 期: 2016年05月31日 15时33分45秒
 * 作 者: Administrator
 * 程序说明:8X8LED模块循环显示Led_Power_Code[8]中的数据。
 * 适用芯片: KF8FXXX系列——KF8F2156、KF8F3156、KF8F4156
 * 			KF8TSXXXX系列——KF8TS2716、KF8TS2516(4x8LED)
 * 			KF8VXXX系列——KF8V327、KF8V427、KF8V429
 ****************************************************************************************/
#include<KF8TS2716.h>
//定义段码
char const smg_arr[] =         {
								  0Xa0,  //0
						          0Xbe,  //1
						          0X62,  //2
						          0X2a,  //3
						          0X3c,  //4
						          0X29,  //5
						          0X21,  //6
						          0Xba,  //7
						          0X20,  //8
						          0X28,  //9
						          0xff,  //关
						          0xdf,	//:
					        	};
int const Led_Power_Code[8] = 	{
		                         0X00C8,  //200
						 	 	 0X0190,  //400
						 	 	 0X0320,  //800
						 	 	 0X03E8,  //1000
						 	 	 0X0514,  //1300
						 	 	 0X0640,  //1600
						 	 	 0X0708,  //1800
						 	 	 0X0834,  //2100
							    };
int display_data;
unsigned int  temp_data;
unsigned char unint,decade,hundred,thound;
//;************************************************************************************
//;* 函 数 名:  delay_ms
//;* 函数功能: 延时函数
//;* 入口参数: 无
//;* 返    回:  无
//;************************************************************************************
void delay_ms(int ms_data)
{
	int j=0;
	while(ms_data--)
	{
		_CWDT();
		j=100;
		while(j--);
	}
}
/****************************************************************************************
 * 函数名：   init_mcu
 * 函数功能：mcu初始化函数
 * 入口参数：无
 * 返回：       无
 ****************************************************************************************/
void init_mcu()
{
	/***时钟初始化****/
	OSCCTL = 0x60;          //设置为8M
	/***端口初始化****/
	TR0 = 0X04;            //P02设置为输入，其余口为输出
	TR1 = 0x00;            //P1设置为输出
	TR2 = 0X00;            //P2设置为输出
	TR3 = 0X00;            //P3设置为输出

    P0LR=0x00;				//P0输出低
    P1LR=0x00;              //P1输出低
    P2LR=0x00;				//P2输出低
    P3LR=0x00;				//P3输出低

    P0=0x00;
    P1=0x00;
    P2=0x00;
    P3=0x00;
}
/****************************************************************************************
 * 函数名：   init_led()
 * 函数功能：LED初始化函数
 * 入口参数：无
 * 返回：       无
 ****************************************************************************************/
void init_led()
{
	LEDOMS0 = 0x00;    //推挽输出
	LEDOMS1 = 0x00;    //推挽输出

	LEDPRE = 0x60;     //LED时钟源选择1:64分频；LED预分频比选择1:1 500hz
	LEDDATA0 = 0XFF;
	LEDDATA2 = 0XFF;
	LEDDATA3 = 0XFF;
	LEDDATA4 = 0XFF;

	LEDLUM = 0x05;	 //设置辉度6:16
	LEDCTL = 0x90;   //打开LED模块，开通LEDDAT0--LEDDAT4;选择共阴数码管
}
/****************************************************************************************
 * 函数名：   display_LED()
 * 函数功能：LED显示
 * 入口参数：无
 * 返回：       无
 ****************************************************************************************/
void display_LED()
{
	DIVAH = display_data>>8;
	DIVAL = display_data;
	DIVB = 10;
	DIVEN = 1;
	while(!DIVOEN);
	unint = DIVR;   //	取余数
	temp_data = DIVQH;
	temp_data = (temp_data<<8) + DIVQL;
	DIVAH = temp_data>>8;
	DIVAL = temp_data;
	DIVB = 10;
	DIVEN = 1;
	while(!DIVOEN);
	decade = DIVR;   //	取余数
	temp_data = DIVQH;
	temp_data = (temp_data<<8) + DIVQL;
	DIVAH = temp_data>>8;
	DIVAL = temp_data;
	DIVB = 10;
	DIVEN = 1;
	while(!DIVOEN);
	hundred = DIVR;
	temp_data = DIVQH;
	temp_data = (temp_data<<8) + DIVQL;
	DIVAH = temp_data>>8;
	DIVAL = temp_data;
	DIVB = 10;
	DIVEN = 1;
	while(!DIVOEN);
	thound = DIVR;
	LEDDATA0 = smg_arr[thound];
	LEDDATA2 = smg_arr[hundred];
	LEDDATA3 = smg_arr[decade];
	LEDDATA4 = smg_arr[unint];
}
//主函数
void main()
{
	int i=0;
	init_mcu();
	init_led();
	while(1)
	{
		if(i<7)
		{
			i++;
		}
		else
		{
			i=0;
		}
		display_data = Led_Power_Code[i];
		delay_ms(2000);
		display_LED();
	}
}


