/*
 * DATA.h
 *
 *  Created on: 2016-5-11
 *      Author: Administrator
 */

#ifndef DATA_H_
#define DATA_H_

typedef         unsigned  char		uchar8;			//定义新数据结构
typedef         unsigned  int		uint16;			//定义新数据结构


typedef	struct
{
	uchar8		b0:1;
	uchar8		b1:1;
	uchar8		b2:1;
	uchar8		b3:1;
	uchar8		b4:1;
	uchar8		b5:1;
	uchar8		b6:1;
	uchar8		b7:1;
}BITstruct;

typedef  union
{
	 uchar8	 AllByte;
	 BITstruct OneBit;
}UBIT;






#endif /* DATA_H_ */
