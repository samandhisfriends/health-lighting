/*
 * extern.h
 *
 *  Created on: 2016-5-12
 *      Author: Administrator
 */

#ifndef EXTERN_H_
#define EXTERN_H_


extern uchar8		Com1Temp;
extern uchar8		Com2Temp;
extern uchar8		Com3Temp;
extern uchar8		Com4Temp;

extern	UBIT		LcdSfr;
#define				SHOW_TN_SEG_P1			LcdSfr.OneBit.b0
#define				SHOW_TN_SEG_2P			LcdSfr.OneBit.b1
#define				SHOW_TN_SEG_3P			LcdSfr.OneBit.b2
#define				SHOW_TN_SEG_4P			LcdSfr.OneBit.b3
#define				CHANGE_SHOW_NUM_FLAG	LcdSfr.OneBit.b4




extern		const	uchar8		BCD[];

extern		uchar8		LcdNumQian;
extern		uchar8		LcdNumBai;
extern		uchar8		LcdNumTen;
extern		uchar8		LcdNumGe;





extern		void	ComTempSetFun();
extern		void	ComScanFun(uchar8 ComScanMode);
#endif /* EXTERN_H_ */
