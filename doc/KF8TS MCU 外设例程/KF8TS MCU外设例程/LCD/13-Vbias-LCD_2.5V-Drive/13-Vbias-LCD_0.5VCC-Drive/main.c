/****************************************************************************************
 *
 * 文  件  名: main.c
 * 项  目  名: 13-Vbias-LCD_0.5VCC-Drive
 * 版         本: v1.0
 * 日         期: 2016年05月16日 11时38分22秒
 * 作         者: Administrator
 * 程序说明：Vbias偏压输出例程
 * 			——驱动LCD液晶显示屏
 * 适用芯片：KF8FXXXX系列——KF8F2156、KF8F3156、KF8F4156
 * 			KF8TS27XX系列——KF8TS2516、KF8TS2716
 * 			KF8VXXX系列——KF8V327、KF8V427、KF8V429
 ****************************************************************************************/
#include"ALL.h"

uchar8		Time0Cnt;
uchar8		LcdModeCnt;


//;****************************************************************************************
//;* 函  数   名: RamClear
//;* 函数功能: 初始化内存，bank0通用 全部清零
//;* 入口参数: 无
//;* 返         回: 无
//;****************************************************************************************
void		RamClear()
{
	__asm
    MOV      R1,#0X80
    MOV      R2,#0X00
    ST    [R1],R2
    INC      R1
    MOV      R0,R1
    XOR      R0,#0XFF
    JB    PSW,2
    JMP      $-5
    __endasm;


}




/********************************************
 *函数名称：T0TimeTurnOnFun
 *函数功能：开启定时器0
 *定时时间：1ms
 *输入参数：无
 *输出参数：无
 * **********************************************/
void   T0TimeTurnOnFun()
{
	T0CS = 0;
	PSA = 0;
	PS2 = 1;
	PS1 = 1;
	PS0 = 1;
	T0=	240;
	T0IE = 1;
	AIE = 1;
}




/********************************************
 *函数名称：initFun
 *函数功能：初始化时钟和IO
 *输入参数：无
 *输出参数：无
 * **********************************************/
void initFun()
{
	OSCCTL	= OSCCTL_16M;

	TR0		= TR0_INIT;				//P0.2 mode引脚只能为输入状态
	TR1		= TR1_INIT;
	TR2		= TR2_INIT;			//其他口都设置为了输出状态
	TR3		= TR3_INIT;

	P0LR	= P0LR_INIT;
	P1LR	= P1LR_INIT;			//初始输出状态
	P2LR	= P2LR_INIT;
	P3LR	= P3LR_INIT;

	VBIASCTL = 0b10000011;		//允许半电平输出

}

//主函数
void main()
{
	initFun();
	RamClear();

	LcdNumQian=BCD[1];		//给千位赋笔形码
	LcdNumBai =BCD[2];		//给百位赋笔形码
	LcdNumTen =BCD[3];		//给十位赋笔形码
	LcdNumGe  =BCD[4];		//给个位赋笔形码

	SHOW_TN_SEG_P1=0;
	SHOW_TN_SEG_2P=0;
	SHOW_TN_SEG_3P=0;
	SHOW_TN_SEG_4P=0;

	CHANGE_SHOW_NUM_FLAG = 1;  //完成一次数据改变//每次修改为数据后，将数据改变标志设置为1,数据显示函数获取后改变显示，并且清零该位

	T0TimeTurnOnFun();
	while(1);

}
//中断函数0:0X04入口地址
void int_fun0() __interrupt (0)
{
	if(T0IF)				//定时中断   1ms
	{
		T0IF = 0;
		T0=	240;
		if(Time0Cnt++>=2)	//3MS
		{
			Time0Cnt=0;



			ComTempSetFun();
			ComScanFun(LcdModeCnt);
			if(++LcdModeCnt>COM4_MODE_H)		//LCD循环扫描
			{
				LcdModeCnt = COM1_MODE_L;
			}

		}
	}
}


//中断函数1:0x14入口地址
void int_fun1() __interrupt (1)
{

}
