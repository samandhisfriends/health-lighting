/*
 * LCD.H
 *
 *  Created on: 2016-5-12
 *      Author: Administrator
 */

#ifndef LCD_H_
#define LCD_H_


//COM扫描模式
#define			COM1_MODE_L				0
#define			COM1_MODE_H				1


#define			COM2_MODE_L				2
#define			COM2_MODE_H				3


#define			COM3_MODE_L				4
#define			COM3_MODE_H				5



#define			COM4_MODE_L				6
#define			COM4_MODE_H				7
//COM扫描模式

//COM设置电平
#define		COM1_SET_H		VBS13EN =0;TR04 =0;P0LR4=1;

#define		COM2_SET_H		VBS12EN =0;TR03 =0;P0LR3=1;

#define		COM3_SET_H		VBS11EN =0;TR01 =0;P0LR1=1;

#define		COM4_SET_H		VBS10EN =0;TR00 =0;P0LR0=1;



#define		COM1_SET_L		VBS13EN =0;TR04 =0;P0LR4=0;

#define		COM2_SET_L		VBS12EN =0;TR03 =0;P0LR3=0;

#define		COM3_SET_L		VBS11EN =0;TR01 =0;P0LR1=0;

#define		COM4_SET_L		VBS10EN =0;TR00 =0;P0LR0=0;
//COM设置电平

#endif /* LCD_H_ */
