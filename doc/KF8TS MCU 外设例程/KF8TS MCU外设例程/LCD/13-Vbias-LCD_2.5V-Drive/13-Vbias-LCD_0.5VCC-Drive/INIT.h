/*
 * INIT.h
 *
 *  Created on: 2016-5-11
 *      Author: Administrator
 */

#ifndef INIT_H_
#define INIT_H_


//#define			DEBUG				//如果要启动调试  .将该宏恢复，如果不用调试，请将该宏注释


//IO口宏定义
#define			DIS_IO_SEG_1		P3LR0
#define			DIS_IO_SEG_2		P3LR1
#define			DIS_IO_SEG_3		P3LR2
#define			DIS_IO_SEG_4		P3LR3
#define			DIS_IO_SEG_5		P3LR4
#define			DIS_IO_SEG_6		P3LR5
#define			DIS_IO_SEG_7		P3LR6
#define			DIS_IO_SEG_8		P3LR7

#define			SWITCH_KEY			P13			//切换按键
#define			CHANGE_KEY			P26			//修改按键




//IO口宏定义





#define			OSCCTL_16M			0b01110000		// CLKOE=0不输出时钟        IRCS[2-0]=111 1:1分频     SCS1[1,0]=00选择内部时钟  IESO=0 禁止双速 FSCM=0 禁止故障检测

#ifdef			DEBUG
#define			TR0_INIT			0b00000111
#else
#define			TR0_INIT			0b00000100
#endif

#define			TR1_INIT			0b00001000
#define			TR2_INIT			0b01000000
#define			TR3_INIT			0b00000000

#define			P0LR_INIT			0b00000000
#define			P1LR_INIT			0b00000000
#define			P2LR_INIT			0b00000000
#define			P3LR_INIT			0b00000000



#endif /* INIT_H_ */
