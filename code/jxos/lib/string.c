

#include "type.h"

uint8_t memcmp(uint8_t *str1, uint8_t *str2, uint32_t n)
{
    if (!n)
        return 0;
    while ( --n && *str1 == *str2 ) {
        str1++;
        str2++;
    }

    n = *str1;
    n -= *str2;
    return (int32_t)n;
}

void memcpy(uint8_t *dest, const uint8_t *src, uint32_t n)
{
    if (!n)
        return;
	while(n--){
		*dest = *src;
		dest++;
		src++;
	}
}

void memset(uint8_t *str, uint8_t c, uint32_t n)
{
    if (!n)
        return;
	while(n--){
		*str = c;
		str++;
	}
}

uint32_t strlen(char *str)
{
	uint32_t n = 0;
	while(1){
		if(*str == 0){
			break;
		}
		n++;
		str++;
	}
    return n;
}
