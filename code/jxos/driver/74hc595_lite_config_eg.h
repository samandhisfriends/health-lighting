


#define hc_74hc595_sclr_high_config()       //in put data register enable (high level)
#define hc_74hc595_sclr_low_config()         //in put data register set to 0 (low level)

#define hc_74hc595_sck_high_config()        //in put data register shift a bit (Qh->move out, Qg->Qh ... Qa->Qb, new bit->Qa) (rising edge)
#define hc_74hc595_sck_low_config()         //in put data register lock (falling edge)

#define hc_74hc595_rck_high_config()        //out put data register reload (rising edge)
#define hc_74hc595_rck_low_config()          //out put data register lock (falling edge)

#define hc_74hc595_oe_high_config()         //out put pin disable (high level)
#define hc_74hc595_oe_low_config()           //out put pin enable (low level)

#define hc_74hc595_si_high_config()         //in put bit 1
#define hc_74hc595_si_low_config()          //in put bit 0

#define hc_74hc595_delay_config()
