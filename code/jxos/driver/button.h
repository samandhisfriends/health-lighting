
#ifndef __BUTTON__H
#define __BUTTON__H

#include "../lib/type.h"

void button_init(void);
void button_scan_handler(void);
bool_t button_read_press_release(uint8_t button_mun);
bool_t button_all_release(void);

#endif //
