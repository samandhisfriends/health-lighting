
#include "../lib/type.h"
#include "button_config.h"

#ifndef BUTTON_NUM_MAX
#define BUTTON_NUM_MAX 		        1
#warning BSP_KEY DONT HAVE A UESER CONFIG,USEING DEFUALT CONFIG
#endif 
#ifndef BUTTON_PRESS_LEVEL
#define BUTTON_PRESS_LEVEL				0
#endif 
#ifndef BUTTON_JITTER_TICK
#define BUTTON_JITTER_TICK					0
#endif 
#ifndef BUTTON_LONG_PRESS_TICK
#define BUTTON_LONG_PRESS_TICK 		    100
#endif
#ifndef BUTTON_LONG_PRESS_REPEAT_TICK
#define BUTTON_LONG_PRESS_REPEAT_TICK 		100
#endif

#define BUTTON_RELEASE_LEVEL  (!BUTTON_PRESS_LEVEL)

#define button_press_count			0
#define button_last_pin_level		1

static volatile uint16_t KEYS[BUTTON_NUM_MAX*2];

//press 1; release 0
bool_t button_read_press_release(uint8_t button_mun)
{
	if(button_mun < BUTTON_NUM_MAX){
		return (button_read_pin_level_config(button_mun) == BUTTON_PRESS_LEVEL);
	}
	else{
		return BUTTON_RELEASE_LEVEL;
	}
}

bool_t button_all_release(void)
{
	uint8_t i;
	bool_t ret = 1;

	for (i = 0; i < BUTTON_NUM_MAX; i++) {
		if(button_read_pin_level_config(i) != BUTTON_RELEASE_LEVEL){
			ret = 0;
			break;
		}
	}
	
	return ret;
}

void button_scan_handler(void)
{
	uint8_t temp_button_pin_level;
	uint8_t i;
	uint8_t j;
	for (i = 0; i < BUTTON_NUM_MAX; i++) {
		j = i*2;
		temp_button_pin_level = button_read_pin_level_config(i);
        if(KEYS[j+button_last_pin_level] == BUTTON_RELEASE_LEVEL){
            if(temp_button_pin_level == BUTTON_RELEASE_LEVEL){
                KEYS[j+button_press_count] = 0;
				button_free_event(i);
            }
            else{
			  	if(KEYS[j+button_press_count] == 0){
					button_press_event(i);
				}
                KEYS[j+button_press_count]++;
            }
        }
        else{
            if(temp_button_pin_level == BUTTON_RELEASE_LEVEL){
#if (BUTTON_JITTER_TICK > 0)
				if(KEYS[j+button_press_count] >= BUTTON_JITTER_TICK){
#endif
						button_release_event(i);
#if (BUTTON_JITTER_TICK > 0)
				}
#endif
                KEYS[j+button_press_count] = 0;
            }
            else{
                KEYS[j+button_press_count]++;
                if(KEYS[j+button_press_count] == BUTTON_LONG_PRESS_TICK){
					button_long_press_event(i);
                }
				else if(KEYS[j+button_press_count] >= BUTTON_LONG_PRESS_TICK + BUTTON_LONG_PRESS_REPEAT_TICK){
					button_long_press_repeat_event(i);
					KEYS[j+button_press_count] = BUTTON_LONG_PRESS_TICK;
				}
            }
        }
        KEYS[j+button_last_pin_level] = temp_button_pin_level;
	}
}

void button_init(void)
{
	uint8_t i;
	uint8_t j;
    for(i = 0; i < BUTTON_NUM_MAX; i++) {
		j = i*2;
        KEYS[j+button_press_count] = 0;
        KEYS[j+button_last_pin_level] = BUTTON_RELEASE_LEVEL;
	}
	button_hal_init_config();
}

