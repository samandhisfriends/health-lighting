
#ifndef __standard_app_value_move_task_H
#define __standard_app_value_move_task_H

#include "../lib/type.h"

void standard_app_value_move_task_init(void);
void standard_app_value_move_task(void);

#endif
