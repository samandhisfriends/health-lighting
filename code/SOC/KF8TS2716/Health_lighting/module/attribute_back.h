

#ifndef __ATTR_H
#define __ATTR_H

#include "lib/type.h"

void attribute_store(void);
void attribute_recover(void);

#endif
