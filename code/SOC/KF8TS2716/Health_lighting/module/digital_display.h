

#ifndef __DIGITAL_H
#define __DIGITAL_H
//Nixie tube
#include "lib/type.h"

void digital_display_init(void);
void digital_display_single(uint8_t bit, uint8_t dis_data);
void digital_display_show(uint16_t num);
void digital_display_set_lum(uint8_t lum); //0 ~ 7

#endif 
