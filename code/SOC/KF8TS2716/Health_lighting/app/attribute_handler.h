
#ifndef __ATTR_HANDLER_H
#define __ATTR_HANDLER_H

#include "lib/type.h"

void attribute_set_color_temperature(uint16_t ct);
void attribute_move_color_temperature(bool_t up_down) ;      //1 up; 0 down;
void attribute_color_temperature_stop_move(void);

void attribute_set_light_level(uint8_t light);
void attribute_move_light_level(bool_t up_down);       //1 up; 0 down;
void attribute_light_level_stop_move(void);

void attribute_set_fan_level(uint8_t fan);

void attribute_save_current_attributer(void);			//current -> last
void attribute_save_color_temperature(uint16_t s_temp);	//parame -> last
void attribute_save_light_level(uint16_t s_level);		//parame -> last
void attribute_recover_current_attributer(void);		//last -> current

void attribute_save_set_attributer_now(void);

void led_set_color(uint8_t r, uint8_t g, uint8_t b);
void led_set_speed(uint16_t ms);

void display_trun_off(void);
void display_trun_on(void);


#endif
