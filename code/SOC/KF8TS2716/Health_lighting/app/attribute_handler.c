

#include "jxos_public.h"
#include "led.h"
#include "light.h"
#include "fan.h"
#include "digital_display.h"
#include "cct.h"

//light handler + fan handler + led handler -> attribute_handler

#define PWM_LED_R				0
#define PWM_LED_G				1
#define PWM_LED_B				2
#define LIGHT_LEVEL_VALUE		3
#define CT_VALUE		        4
#define DISPLAY_LUM				5

#define DEFAULT_COLOR_TEMPERATURE		4000
#define DEFAULT_LIGHT_LEVEL				100
#define DEFAULT_FAN_LEVEL				100
#define DEFAULT_SLEEP_TIME				1

static uint16_t last_color_temperature;
static uint8_t last_light_level;

uint16_t target_color_temperature;
uint8_t target_light_level;
static uint8_t target_fan_level;

static uint16_t current_color_temperature;
static uint8_t current_light_level;
static uint8_t current_fan_level;

/****************************************************************/
void attribute_set_color_temperature(uint16_t ct)
{
    if((ct >= 2700)&&(ct <= 6500)){
        standard_app_value_move_task_move_to(CT_VALUE, ct, 500);
    }
}

void attribute_set_light_level(uint8_t light)
{
    if(light <= 100){
        standard_app_value_move_task_move_to_by_step(LIGHT_LEVEL_VALUE, light, 1);
    }
}

void attribute_move_color_temperature(bool_t up_down)       //1 up; 0 down;
{
    if(up_down){
        standard_app_value_move_task_circle_move(CT_VALUE, 6500, 2700, 5000);
    }
    else{
        standard_app_value_move_task_circle_move(CT_VALUE, 2700, 6500, 5000);
    }
}

void attribute_move_light_level(bool_t up_down)       //1 up; 0 down;
{
    if(up_down){
        standard_app_value_move_task_circle_move_by_step(LIGHT_LEVEL_VALUE, 100, 0, 1);
    }
    else{
        standard_app_value_move_task_circle_move_by_step(LIGHT_LEVEL_VALUE, 0, 100, 1);
    }
}

void attribute_color_temperature_stop_move(void)
{
    standard_app_value_move_task_stop_move(CT_VALUE);
}

void attribute_light_level_stop_move(void)
{
    standard_app_value_move_task_stop_move(LIGHT_LEVEL_VALUE);
}

/****************************************************************/
void attribute_set_fan_level(uint8_t fan)
{
    if(fan <= 100){
    	target_fan_level = fan;
    }
}

/****************************************************************/
void led_set_color(uint8_t r, uint8_t g, uint8_t b)
{
	standard_app_value_move_task_move_to(PWM_LED_R, r, 2000);
	standard_app_value_move_task_move_to(PWM_LED_G, g, 2000);
	standard_app_value_move_task_move_to(PWM_LED_B, b, 2000);
}

/****************************************************************/
void display_trun_off(void)
{
	standard_app_value_move_task_move_to(DISPLAY_LUM, 0, 2000);
}

void display_trun_on(void)
{
	standard_app_value_move_task_move_to(DISPLAY_LUM, 7, 2000);
}

/****************************************************************/
void attribute_save_current_attributer(void)	//current -> last
{
    if(current_light_level != 0){
        last_light_level = current_light_level;
    }
    last_color_temperature = current_color_temperature;
}

void attribute_save_color_temperature(uint16_t s_temp)	//parame -> last
{
    last_color_temperature = s_temp;
}

void attribute_save_light_level(uint16_t s_level)	//parame -> last
{
    if(s_level != 0){
        last_light_level = s_level;
    }
}

void attribute_recover_current_attributer(void)	//last -> current
{
	attribute_set_light_level(last_light_level);
	attribute_set_color_temperature(last_color_temperature);
}

/****************************************************************/
void attribute_handler_task(void)
{
    uint8_t temp1;
    uint8_t temp2;
    uint16_t temp3;
   
    if((current_color_temperature != target_color_temperature)
        ||(current_light_level != target_light_level)){
        current_color_temperature = target_color_temperature;
        current_light_level = target_light_level;

		//  printf_16bit_hex(current_color_temperature);
		//  printf_string(" change ct\r\n");
		//  printf_16bit_hex(current_light_level);
		//  printf_string(" change level\r\n");

        if(cct_get_duty(current_color_temperature, &temp1, &temp2)){
            temp3 = temp1;
            temp3 = (temp3*current_light_level)/100;
            pwm_c_light_set((uint8_t)temp3);

            // printf_byte_hex((uint8_t)temp3);
            // printf_string(" ");

            temp3 = temp2;
            temp3 = (temp3*current_light_level)/100;
            pwm_w_light_set((uint8_t)temp3);

            // printf_byte_hex((uint8_t)temp3);
            // printf_string("\r\n");
        }
    }

    if(current_fan_level != target_fan_level){
        current_fan_level = target_fan_level;
        pwm_fan_set(current_fan_level);
    }
}

void attribute_handler_task_init(void)
{
	led_init();
	pwm_led_init();
	pwm_led_r_set(0);
	pwm_led_g_set(0);
	pwm_led_b_set(0);
	led_set_color(5,5,5);

	digital_display_init();
	display_trun_on();
    digital_display_show(0);

    light_init();
    pwm_light_init();
    pwm_c_light_set(0);
    pwm_w_light_set(0);
    current_color_temperature = DEFAULT_COLOR_TEMPERATURE;
    current_light_level = 0;
    standard_app_value_move_task_move_to(CT_VALUE, current_color_temperature, 0);
    standard_app_value_move_task_move_to(LIGHT_LEVEL_VALUE, current_light_level, 0);

    fan_init();
    pwm_fan_init();
    current_fan_level = 0;
    pwm_fan_set(0);

    attribute_set_color_temperature(DEFAULT_COLOR_TEMPERATURE);
    attribute_set_light_level(DEFAULT_LIGHT_LEVEL);
    attribute_set_fan_level(100);
}


