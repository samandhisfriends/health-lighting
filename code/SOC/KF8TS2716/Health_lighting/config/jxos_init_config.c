
#include "jxos_public.h"

void mcu_init(void);
void sys_software_timer_task_hal_init_callback_handler(void);
void sys_software_timer_task_hal_start_callback_handler(void);
void sys_software_timer_task_hal_stop_callback_handler(void);
void jxos_hal_init(void)
{
	mcu_init();

	sys_software_timer_task_hal_init_callback = sys_software_timer_task_hal_init_callback_handler;
	sys_software_timer_task_hal_start_callback = sys_software_timer_task_hal_start_callback_handler;
	sys_software_timer_task_hal_stop_callback = sys_software_timer_task_hal_stop_callback_handler;

//	sys_debug_print_task_hal_init_callback = sys_debug_print_task_hal_init_callback_handler;
//	sys_debug_print_task_send_finish_check_callback = sys_debug_print_task_send_finish_check_callback_handler;
//	sys_debug_print_task_send_byte_callback = sys_debug_print_task_send_byte_callback_handler;

//	standard_app_key_task_hal_init_callback = standard_app_key_task_hal_init_callback_handler;
//	standard_app_key_task_hal_read_pin_level_callback = standard_app_key_task_hal_read_pin_level_callback_handler;

}

void button_handler_task_init(void);
void attribute_handler_task_init(void);
void tick_handler_task_init(void);
void jxos_user_task_init(void)
{
	button_handler_task_init();
	attribute_handler_task_init();
	tick_handler_task_init();
}


void mcu_run(void);
void jxos_prepare_to_run(void)
{
	mcu_run();
}


void button_handler_task(void);
void attribute_handler_task(void);
void tick_handler_task(void);
void jxos_user_task_run(void)
{
	button_handler_task();
	attribute_handler_task();
	tick_handler_task();
}

